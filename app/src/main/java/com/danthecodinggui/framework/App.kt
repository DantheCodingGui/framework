package com.danthecodinggui.framework

import android.app.Application
import com.danthecodinggui.framework.core.ReleaseTree
import com.danthecodinggui.framework.di.application.ApplicationComponent
import com.danthecodinggui.framework.di.application.ApplicationModule
import com.danthecodinggui.framework.di.application.DaggerApplicationComponent
import com.facebook.stetho.Stetho
import timber.log.Timber

class App: Application() {

//    @Inject
//    lateinit var workerFactory: AppWorkerFactory

    val applicationComponent: ApplicationComponent by lazy {
        DaggerApplicationComponent.builder()
            .applicationModule(ApplicationModule(this))
            .build()
    }

    override fun onCreate() {
        applicationComponent.inject(this)
        super.onCreate()
        setupLogger()

        // WorkManager.initialize(this, Configuration.Builder().setWorkerFactory(workerFactory).build())
        Stetho.initializeWithDefaults(this)
    }

    private fun setupLogger() {
        if (BuildConfig.DEBUG)
            Timber.plant(Timber.DebugTree())
        else
            Timber.plant(ReleaseTree())
    }
}