package com.danthecodinggui.framework.di.injectables

import androidx.appcompat.app.AppCompatActivity
import com.danthecodinggui.framework.App
import com.danthecodinggui.framework.di.view.ViewComponent
import com.danthecodinggui.framework.di.view.ViewModule

/**
 * Created by Dan on 31/01/2020
 */
abstract class DaggerAppCompatActivity: AppCompatActivity() {

    protected val viewComponent: ViewComponent by lazy {
        (application as App)
            .applicationComponent
            .viewComponent(ViewModule())
    }
}