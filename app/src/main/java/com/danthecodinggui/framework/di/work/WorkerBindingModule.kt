package com.danthecodinggui.framework.di.work

import androidx.work.ListenableWorker
import dagger.MapKey
import dagger.Module
import kotlin.reflect.KClass

@Module
interface WorkerBindingModule

@MapKey
@Target(AnnotationTarget.FUNCTION)
@Retention(AnnotationRetention.RUNTIME)
annotation class WorkerKey(val value: KClass<out ListenableWorker>)