package com.danthecodinggui.framework.di.application

import android.app.Application
import android.content.Context
import dagger.Module
import dagger.Provides

@Module
class ApplicationModule(private val application: Application) {

    @ApplicationScope
    @Provides
    fun providesApplication(): Application = application

    @ApplicationScope
    @Provides
    fun providesApplicationContext(): Context = application.applicationContext
}