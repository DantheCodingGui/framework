package com.danthecodinggui.framework.di.other

import android.content.Context
import com.danthecodinggui.framework.di.application.ApplicationScope
import com.danthecodinggui.framework.repository.system_services.SystemServices
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import dagger.Module
import dagger.Provides

@Module
class OtherGlobalModule {

    @ApplicationScope
    @Provides
    fun provideMoshi(): Moshi = Moshi.Builder()
        .add(KotlinJsonAdapterFactory())
        .build()

    @ApplicationScope
    @Provides
    fun provideSystemServices(context: Context): SystemServices = SystemServices(context)
}