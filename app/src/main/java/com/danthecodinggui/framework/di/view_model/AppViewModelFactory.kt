package com.danthecodinggui.framework.di.view_model

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.danthecodinggui.framework.di.view.ViewScope
import javax.inject.Inject
import javax.inject.Provider

@ViewScope
class AppViewModelFactory @Inject constructor(
    private val viewModels: Map<Class<out ViewModel>, @JvmSuppressWildcards Provider<ViewModel>>
): ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        val foundEntry = viewModels.entries.find { modelClass.isAssignableFrom(it.key) }
        val provider = foundEntry?.value ?: throw IllegalArgumentException("Unknown worker class name: ${modelClass.simpleName}")
        return provider.get() as T
    }
}