package com.danthecodinggui.framework.di.view_model

import androidx.lifecycle.ViewModel
import com.danthecodinggui.framework.di.view.ViewScope
import com.danthecodinggui.framework.features.ExampleViewModel
import dagger.Binds
import dagger.MapKey
import dagger.Module
import dagger.multibindings.IntoMap
import kotlin.reflect.KClass

@Module
interface ViewModelBindingModule {

    @ViewScope
    @Binds
    @IntoMap
    @ViewModelKey(ExampleViewModel::class)
    fun bindExampleViewModel(exampleViewModel: ExampleViewModel): ViewModel

}

@MapKey
@Target(AnnotationTarget.FUNCTION)
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
internal annotation class ViewModelKey(val value: KClass<out ViewModel>)