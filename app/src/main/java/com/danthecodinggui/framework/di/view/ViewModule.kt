package com.danthecodinggui.framework.di.view

import android.content.Context
import com.danthecodinggui.framework.util.ErrorParser
import com.danthecodinggui.framework.ResourceProvider
import dagger.Module
import dagger.Provides

@Module
class ViewModule {

    @ViewScope
    @Provides
    fun provideResourceProvider(appContext: Context): ResourceProvider = ResourceProvider(appContext)

    @ViewScope
    @Provides
    fun provideErrorParser(resourceProvider: ResourceProvider) = ErrorParser(resourceProvider.strings)
}