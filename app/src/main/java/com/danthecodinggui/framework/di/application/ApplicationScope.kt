package com.danthecodinggui.framework.di.application

import javax.inject.Scope

/**
 * Global Scope, equivalent to @Singleton (better semantic meaning)
 */
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class ApplicationScope