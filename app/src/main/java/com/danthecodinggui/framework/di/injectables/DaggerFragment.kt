package com.danthecodinggui.framework.di.injectables

import androidx.fragment.app.Fragment
import com.danthecodinggui.framework.App
import com.danthecodinggui.framework.di.view.ViewComponent
import com.danthecodinggui.framework.di.view.ViewModule

abstract class DaggerFragment: Fragment() {

    protected val viewComponent: ViewComponent by lazy {
        (requireActivity().application as App)
            .applicationComponent
            .viewComponent(ViewModule())
    }
}