package com.danthecodinggui.framework.di.view

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class ViewScope