package com.danthecodinggui.framework.di.view

import com.danthecodinggui.framework.di.view_model.ViewModelBindingModule
import com.danthecodinggui.framework.features.ExampleActivity
import dagger.Subcomponent

@ViewScope
@Subcomponent(modules = [
    ViewModule::class,
    ViewModelBindingModule::class
])
interface ViewComponent {

    fun inject(mainActivity: ExampleActivity)
}