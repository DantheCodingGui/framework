package com.danthecodinggui.framework.di.application

import com.danthecodinggui.framework.App
import com.danthecodinggui.framework.di.other.OtherGlobalModule
import com.danthecodinggui.framework.di.other.RepositoryModule
import com.danthecodinggui.framework.di.view.ViewComponent
import com.danthecodinggui.framework.di.view.ViewModule
import com.danthecodinggui.framework.di.work.WorkerBindingModule
import dagger.Component

@ApplicationScope
@Component(modules = [
    ApplicationModule::class,
    RepositoryModule::class,
    OtherGlobalModule::class,
    WorkerBindingModule::class
])
interface ApplicationComponent {

    fun inject(application: App)

    // Subcomponents
    fun viewComponent(viewModule: ViewModule): ViewComponent
}