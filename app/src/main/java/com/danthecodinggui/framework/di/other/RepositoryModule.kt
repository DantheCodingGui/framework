package com.danthecodinggui.framework.di.other

import android.content.Context
import com.danthecodinggui.framework.di.application.ApplicationScope
import com.danthecodinggui.framework.repository.api.ApiRepository
import com.danthecodinggui.framework.repository.storage.AppDatabase
import com.danthecodinggui.framework.repository.storage.StorageRepository
import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides

@Module
class RepositoryModule {

    @ApplicationScope
    @Provides
    fun provideApiRepository(context: Context, moshi: Moshi): ApiRepository =
        ApiRepository(context, moshi)

    @ApplicationScope
    @Provides
    fun provideStorageRepository(appDatabase: AppDatabase): StorageRepository =
        StorageRepository(appDatabase)

    @ApplicationScope
    @Provides
    fun provideAppDatabase(context: Context): AppDatabase =
        AppDatabase.getInstance(context)
}