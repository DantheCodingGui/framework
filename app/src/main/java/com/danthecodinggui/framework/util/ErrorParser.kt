package com.danthecodinggui.framework.util

import com.danthecodinggui.framework.StringProvider

class ErrorParser(private val stringsProvider: StringProvider) {

    /**
     * Maps an error to a string resource
     * @param error The raw error received
     * @param subject The subject of the error, if non-empty will add to the error string, e.g
     * 'Couldn't find that' vs 'Couldn't find that Movie'
     * @param overrides Allows individual overrides for certain error types, if required in certain
     * scenarios (NOTE: Will completely override default)
     */
    fun parse(error: Error, subject: String = "", overrides: Map<Error, String> = emptyMap()): String {
        return when(error) {
            Error.NETWORK -> overrides[Error.NETWORK] ?: stringsProvider.getErrorNoNetwork()
            Error.TIMEOUT -> overrides[Error.TIMEOUT] ?: stringsProvider.getErrorTimeout()
            Error.RESOURCE_NOT_FOUND -> overrides[Error.RESOURCE_NOT_FOUND] ?: stringsProvider.getErrorResourceNotFound(" $subject")
            Error.SERVER_ERROR -> overrides[Error.SERVER_ERROR] ?: stringsProvider.getErrorServerInternal()
            Error.NOT_AUTHORISED -> overrides[Error.NOT_AUTHORISED] ?: stringsProvider.getErrorNotAuthorised()
            // Error.JSON_PARSING has to be ignored as I can't know what the error was
            else -> overrides[Error.UNKNOWN] ?: stringsProvider.getErrorUnknown()
        }
    }
}