package com.danthecodinggui.framework.util

import android.os.Build

fun atLeastNougat() = Build.VERSION.SDK_INT >= Build.VERSION_CODES.N