package com.danthecodinggui.framework.util

enum class Error {
    NETWORK, // IO
    TIMEOUT, // SOCKET
    RESOURCE_NOT_FOUND, // 404 for network
    SERVER_ERROR, // 500
    NOT_AUTHORISED, // 400
    JSON_PARSING, // Stupid API design, 200 response w/ error message
    UNKNOWN
}