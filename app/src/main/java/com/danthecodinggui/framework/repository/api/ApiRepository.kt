package com.danthecodinggui.framework.repository.api

import android.content.Context
import com.squareup.moshi.Moshi
import okhttp3.Cache
import okhttp3.OkHttpClient
import retrofit2.converter.moshi.MoshiConverterFactory

class ApiRepository(context: Context, moshi: Moshi) {

    private val baseOkHttpClient =
        OkHttpClient.Builder()
            .cache(Cache(context.cacheDir, 5 * 1024 * 1024))
            .build()

    private val jsonConverterFactory: MoshiConverterFactory = MoshiConverterFactory.create(moshi)

    // val exampleRequest: ExampleRequest by lazy { ExampleRequest(baseOkHttpClient, jsonConverterFactory) }
}