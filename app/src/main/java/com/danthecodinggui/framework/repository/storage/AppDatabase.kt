package com.danthecodinggui.framework.repository.storage

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

// @Database(entities = [], version = 1, exportSchema = false)
abstract class AppDatabase: RoomDatabase() {
    // abstract fun exampleDao(): ExampleDao

    companion object {
        @Volatile
        private lateinit var INSTANCE: AppDatabase

        fun getInstance(context: Context): AppDatabase {
            if (!::INSTANCE.isInitialized) {
                synchronized(this) {
                    INSTANCE = Room.databaseBuilder(
                        context.applicationContext,
                        AppDatabase::class.java,
                        "db"
                    ).build()
                }
            }
            return INSTANCE
        }
    }
}