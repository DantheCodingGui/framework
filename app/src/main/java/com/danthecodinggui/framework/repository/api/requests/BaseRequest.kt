package com.danthecodinggui.framework.repository.api.requests

import com.danthecodinggui.framework.repository.api.ApiResponse
import com.danthecodinggui.framework.util.Error
import com.squareup.moshi.JsonDataException
import okhttp3.OkHttpClient
import retrofit2.Converter
import retrofit2.HttpException
import retrofit2.Retrofit
import timber.log.Timber
import java.io.IOException
import java.net.SocketTimeoutException

abstract class BaseRequest<T>(okHttpClient: OkHttpClient,
                              converterFactory: Converter.Factory,
                              baseUrl: String) {
    protected val retrofit: Retrofit = Retrofit.Builder()
        .baseUrl(baseUrl)
        .client(okHttpClient)
        .addConverterFactory(converterFactory)
        .build()

    protected abstract val endpoint: T

    /**
     * Used as a wrapper to actually make call and handle errors if call fails
     * <p>
     * @param call A lambda that returns call to the endpoint
     * @return A wrapper containing a nullable result (what was requested) and an error (default unknown)
     */
    protected suspend inline fun <R> request(crossinline call: suspend () -> R): ApiResponse<R> {
        return try {
            ApiResponse(call())
        } catch (e: Exception) {
            var error = com.danthecodinggui.framework.util.Error.UNKNOWN
            when(e) {
                is HttpException -> {
                    when(e.code()) {
                        400 -> error = Error.NOT_AUTHORISED
                        404 -> error = Error.RESOURCE_NOT_FOUND
                        500 -> error = Error.SERVER_ERROR
                    }
                }
                is JsonDataException -> {
                    // This means that the API returned 200 with an error message (stupid design)
                    // Still have to treat as unknown error as I can't get semantic meaning from an arbitrary string
                    // Plus, I can't attempt to parse an backup error object as call MUST return R
                    error = Error.JSON_PARSING
                }
                is SocketTimeoutException -> error = Error.TIMEOUT
                is IOException -> error = Error.NETWORK
            }
            Timber.e(e, "${javaClass.simpleName} request failed, reason: $error")
            ApiResponse(error = error)
        }
    }
}