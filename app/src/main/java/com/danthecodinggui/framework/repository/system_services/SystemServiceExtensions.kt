package com.danthecodinggui.framework.repository.system_services

import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import com.danthecodinggui.framework.util.atLeastNougat
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.channels.sendBlocking
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import timber.log.Timber

@ExperimentalCoroutinesApi
fun ConnectivityManager.observeChanges(): Flow<ConnectionModel> = callbackFlow {
    val sendModel: (isConnected: Boolean) -> Unit = { isConnected ->
        val isMobileData = activeNetworkInfo?.type == ConnectivityManager.TYPE_MOBILE
        val model =
            ConnectionModel(
                isConnected,
                isMobileData,
                isActiveNetworkMetered
            )

        try {
            sendBlocking(model)
        } catch (e: Exception) {
            Timber.e(e, "Emitting new network state from flow failed")
        }
    }

    val networkCallback = object: ConnectivityManager.NetworkCallback() {
        override fun onAvailable(network: Network) {
            sendModel(true)
        }

        override fun onLost(network: Network) {
            sendModel(false)
        }
    }

    if (atLeastNougat())
        registerDefaultNetworkCallback(networkCallback)
    else {
        registerNetworkCallback(
            NetworkRequest.Builder()
                .addTransportType(NetworkCapabilities.TRANSPORT_CELLULAR)
                .addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
                .build(),
            networkCallback
        )
    }
    awaitClose { unregisterNetworkCallback(networkCallback) }
}