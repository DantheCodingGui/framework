package com.danthecodinggui.framework.repository.api

import com.danthecodinggui.framework.util.Error

data class ApiResponse<T>(val result: T? = null, val error: Error = Error.UNKNOWN)