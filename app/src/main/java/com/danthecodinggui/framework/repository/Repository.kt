package com.danthecodinggui.framework.repository

import com.danthecodinggui.framework.repository.api.ApiRepository
import com.danthecodinggui.framework.repository.storage.StorageRepository
import javax.inject.Inject

class Repository @Inject constructor(
    private val apiRepository: ApiRepository,
    private val storageRepository: StorageRepository
) {
}