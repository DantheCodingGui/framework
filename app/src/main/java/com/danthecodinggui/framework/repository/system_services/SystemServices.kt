package com.danthecodinggui.framework.repository.system_services

import android.accounts.AccountManager
import android.annotation.SuppressLint
import android.app.*
import android.app.admin.DevicePolicyManager
import android.app.job.JobScheduler
import android.app.usage.NetworkStatsManager
import android.app.usage.StorageStatsManager
import android.app.usage.UsageStatsManager
import android.appwidget.AppWidgetManager
import android.bluetooth.BluetoothManager
import android.companion.CompanionDeviceManager
import android.content.Context
import android.content.RestrictionsManager
import android.content.pm.CrossProfileApps
import android.content.pm.LauncherApps
import android.content.pm.ShortcutManager
import android.hardware.ConsumerIrManager
import android.hardware.SensorManager
import android.hardware.camera2.CameraManager
import android.hardware.display.DisplayManager
import android.hardware.input.InputManager
import android.hardware.usb.UsbManager
import android.location.LocationManager
import android.media.AudioManager
import android.media.MediaRouter
import android.media.midi.MidiManager
import android.media.projection.MediaProjectionManager
import android.media.session.MediaSessionManager
import android.media.tv.TvInputManager
import android.net.ConnectivityManager
import android.net.IpSecManager
import android.net.nsd.NsdManager
import android.net.wifi.WifiManager
import android.net.wifi.aware.WifiAwareManager
import android.net.wifi.p2p.WifiP2pManager
import android.net.wifi.rtt.WifiRttManager
import android.nfc.NfcManager
import android.os.*
import android.os.health.SystemHealthManager
import android.os.storage.StorageManager
import android.print.PrintManager
import android.telecom.TelecomManager
import android.telephony.CarrierConfigManager
import android.telephony.SmsManager
import android.telephony.SubscriptionManager
import android.telephony.TelephonyManager
import android.telephony.euicc.EuiccManager
import android.view.WindowManager
import android.view.accessibility.AccessibilityManager
import android.view.accessibility.CaptioningManager
import android.view.inputmethod.InputMethodManager
import android.view.textclassifier.TextClassificationManager
import android.view.textservice.TextServicesManager
import androidx.annotation.RequiresApi

/**
 * Wrapper around all android system services/managers
 */
@SuppressLint("WifiManagerPotentialLeak")
class SystemServices(private val context: Context) {

    /*
     * All lazy initialised system services, if required add extensions outside of class
     * E.g. val example: ExampleManager by lazy { context.getSystemService(Context.EXAMPLE_SERVICE) as ExampleManager }
     */

    val connectivity: ConnectivityManager by lazy { context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager }
    @delegate:RequiresApi(Build.VERSION_CODES.M)
    val networkStats: NetworkStatsManager by lazy { context.getSystemService(Context.NETWORK_STATS_SERVICE) as NetworkStatsManager }
    val nfc: NfcManager by lazy { context.getSystemService(Context.NFC_SERVICE) as NfcManager }
    val notification: NotificationManager by lazy { context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager }
    val nsd: NsdManager by lazy { context.getSystemService(Context.NSD_SERVICE) as NsdManager }
    val power: PowerManager by lazy { context.getSystemService(Context.POWER_SERVICE) as PowerManager }
    val print: PrintManager by lazy { context.getSystemService(Context.PRINT_SERVICE) as PrintManager }
    val restrictions: RestrictionsManager by lazy { context.getSystemService(Context.RESTRICTIONS_SERVICE) as RestrictionsManager }
    val window: WindowManager by lazy { context.getSystemService(Context.WINDOW_SERVICE) as WindowManager }
    val account: AccountManager by lazy { context.getSystemService(Context.ACCOUNT_SERVICE) as AccountManager }
    val alarm: AlarmManager by lazy { context.getSystemService(Context.ALARM_SERVICE) as AlarmManager }
    val accessibility: AccessibilityManager by lazy { context.getSystemService(Context.ACCESSIBILITY_SERVICE) as AccessibilityManager }
    val captioning: CaptioningManager by lazy { context.getSystemService(Context.CAPTIONING_SERVICE) as CaptioningManager }
    val keyguard: KeyguardManager by lazy { context.getSystemService(Context.KEYGUARD_SERVICE) as KeyguardManager }
    val location: LocationManager by lazy { context.getSystemService(Context.LOCATION_SERVICE) as LocationManager }
    val search: SearchManager by lazy { context.getSystemService(Context.SEARCH_SERVICE) as SearchManager }
    val sensor: SensorManager by lazy { context.getSystemService(Context.SENSOR_SERVICE) as SensorManager }
    val storage: StorageManager by lazy { context.getSystemService(Context.STORAGE_SERVICE) as StorageManager }
    @delegate:RequiresApi(Build.VERSION_CODES.O)
    val storageStats: StorageStatsManager by lazy { context.getSystemService(Context.STORAGE_STATS_SERVICE) as StorageStatsManager }
    val wallpaper: WallpaperManager by lazy { context.getSystemService(Context.WALLPAPER_SERVICE) as WallpaperManager }
    val vibrator: ConnectivityManager by lazy { context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager }
    @delegate:RequiresApi(Build.VERSION_CODES.P)
    val ipSec: IpSecManager by lazy { context.getSystemService(Context.IPSEC_SERVICE) as IpSecManager }
    val wifi: WifiManager by lazy { context.getSystemService(Context.WIFI_SERVICE) as WifiManager }
    @delegate:RequiresApi(Build.VERSION_CODES.O)
    val wifiAware: WifiAwareManager by lazy { context.getSystemService(Context.WIFI_AWARE_SERVICE) as WifiAwareManager }
    val wifiP2p: WifiP2pManager by lazy { context.getSystemService(Context.WIFI_P2P_SERVICE) as WifiP2pManager }
    @delegate:RequiresApi(Build.VERSION_CODES.P)
    val wifiRtt: WifiRttManager by lazy { context.getSystemService(Context.WIFI_RTT_RANGING_SERVICE) as WifiRttManager }
    val audio: AudioManager by lazy { context.getSystemService(Context.AUDIO_SERVICE) as AudioManager }
    val mediaRouter: MediaRouter by lazy { context.getSystemService(Context.MEDIA_ROUTER_SERVICE) as MediaRouter }
    val telephony: TelephonyManager by lazy { context.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager }
    @delegate:RequiresApi(Build.VERSION_CODES.LOLLIPOP_MR1)
    val telephonySubscription: SubscriptionManager by lazy { context.getSystemService(Context.TELEPHONY_SUBSCRIPTION_SERVICE) as SubscriptionManager }
    @delegate:RequiresApi(Build.VERSION_CODES.M)
    val carrierConfig: CarrierConfigManager by lazy { context.getSystemService(Context.CARRIER_CONFIG_SERVICE) as CarrierConfigManager }
    val telecom: TelecomManager by lazy { context.getSystemService(Context.TELECOM_SERVICE) as TelecomManager }
    val inputMethod: InputMethodManager by lazy { context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager }
    val textServices: TextServicesManager by lazy { context.getSystemService(Context.TEXT_SERVICES_MANAGER_SERVICE) as TextServicesManager }
    @delegate:RequiresApi(Build.VERSION_CODES.O)
    val textClassification: TextClassificationManager by lazy { context.getSystemService(Context.TEXT_CLASSIFICATION_SERVICE) as TextClassificationManager }
    val appWidget: AppWidgetManager by lazy { context.getSystemService(Context.APPWIDGET_SERVICE) as AppWidgetManager }
    val dropbox: DropBoxManager by lazy { context.getSystemService(Context.DROPBOX_SERVICE) as DropBoxManager }
    val devicePolicy: DevicePolicyManager by lazy { context.getSystemService(Context.DEVICE_POLICY_SERVICE) as DevicePolicyManager }
    val uiMode: UiModeManager by lazy { context.getSystemService(Context.UI_MODE_SERVICE) as UiModeManager }
    val download: DownloadManager by lazy { context.getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager }
    val bluetooth: BluetoothManager by lazy { context.getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager }
    val usb: UsbManager by lazy { context.getSystemService(Context.USB_SERVICE) as UsbManager }
    val launcherApps: LauncherApps by lazy { context.getSystemService(Context.LAUNCHER_APPS_SERVICE) as LauncherApps }
    val input: InputManager by lazy { context.getSystemService(Context.INPUT_SERVICE) as InputManager }
    val display: DisplayManager by lazy { context.getSystemService(Context.DISPLAY_SERVICE) as DisplayManager }
    val user: UserManager by lazy { context.getSystemService(Context.USER_SERVICE) as UserManager }
    val appOps: AppOpsManager by lazy { context.getSystemService(Context.APP_OPS_SERVICE) as AppOpsManager }
    val camera: CameraManager by lazy { context.getSystemService(Context.CAMERA_SERVICE) as CameraManager }
    val consumerInputManager: ConsumerIrManager by lazy { context.getSystemService(Context.CONSUMER_IR_SERVICE) as ConsumerIrManager }
    val tvInput: TvInputManager by lazy { context.getSystemService(Context.TV_INPUT_SERVICE) as TvInputManager }
    @delegate:RequiresApi(Build.VERSION_CODES.LOLLIPOP_MR1)
    val usageStats: UsageStatsManager by lazy { context.getSystemService(Context.USAGE_STATS_SERVICE) as UsageStatsManager }
    val mediaSession: MediaSessionManager by lazy { context.getSystemService(Context.MEDIA_SESSION_SERVICE) as MediaSessionManager }
    val battery: BatteryManager by lazy { context.getSystemService(Context.BATTERY_SERVICE) as BatteryManager }
    val jobScheduler: JobScheduler by lazy { context.getSystemService(Context.JOB_SCHEDULER_SERVICE) as JobScheduler }
    val mediaProjection: MediaProjectionManager by lazy { context.getSystemService(Context.MEDIA_PROJECTION_SERVICE) as MediaProjectionManager }
    @delegate:RequiresApi(Build.VERSION_CODES.M)
    val midi: MidiManager by lazy { context.getSystemService(Context.MIDI_SERVICE) as MidiManager }
    @delegate:RequiresApi(Build.VERSION_CODES.N)
    val hardwareProperties: HardwarePropertiesManager by lazy { context.getSystemService(Context.HARDWARE_PROPERTIES_SERVICE) as HardwarePropertiesManager }
    @delegate:RequiresApi(Build.VERSION_CODES.N_MR1)
    val shortcut: ShortcutManager by lazy { context.getSystemService(Context.SHORTCUT_SERVICE) as ShortcutManager }
    @delegate:RequiresApi(Build.VERSION_CODES.N)
    val systemHealth: SystemHealthManager by lazy { context.getSystemService(Context.SYSTEM_HEALTH_SERVICE) as SystemHealthManager }
    @delegate:RequiresApi(Build.VERSION_CODES.O)
    val companionDeviceManager: CompanionDeviceManager by lazy { context.getSystemService(Context.COMPANION_DEVICE_SERVICE) as CompanionDeviceManager }
    @delegate:RequiresApi(Build.VERSION_CODES.P)
    val crossProfileApps: CrossProfileApps by lazy { context.getSystemService(Context.CROSS_PROFILE_APPS_SERVICE) as CrossProfileApps }
    @delegate:RequiresApi(Build.VERSION_CODES.P)
    val euicc: EuiccManager by lazy { context.getSystemService(Context.EUICC_SERVICE) as EuiccManager }

    val sms: SmsManager by lazy { SmsManager.getDefault() }
}