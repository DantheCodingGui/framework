package com.danthecodinggui.framework.repository.system_services

data class ConnectionModel(val isConnected: Boolean, val isMobileData: Boolean, val isMetered: Boolean)