package com.danthecodinggui.framework.work

import android.content.Context
import androidx.work.CoroutineWorker
import androidx.work.ListenableWorker
import androidx.work.WorkerParameters
import com.danthecodinggui.framework.di.work.ChildWorkerFactory
import timber.log.Timber
import javax.inject.Inject

class ExampleWorker(context: Context, params: WorkerParameters)
    : CoroutineWorker(context, params) {
    override suspend fun doWork(): Result {
        Timber.d("Doing Work!!")
        return Result.success()
    }

    class Factory @Inject constructor(
    ): ChildWorkerFactory {
        override fun create(appContext: Context, params: WorkerParameters): ListenableWorker {
            return ExampleWorker(
                appContext,
                params
            )
        }
    }
}