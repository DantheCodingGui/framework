package com.danthecodinggui.framework.core

import android.util.Log
import org.jetbrains.annotations.NotNull
import timber.log.Timber

class ReleaseTree : @NotNull Timber.Tree() {
    override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
        if (priority == Log.ERROR || priority == Log.WARN)
            ;//Send Message to instabug/crashylitics

        /*
        * I'm assuming the actual logs are handled for you, but if not then i guess think on what
        * would actually be needed in production?
        * */
    }
}
