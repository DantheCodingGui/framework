package com.danthecodinggui.framework.features

import androidx.lifecycle.ViewModel
import javax.inject.Inject

class ExampleViewModel @Inject constructor(): ViewModel() {}