package com.danthecodinggui.framework.features

import android.os.Bundle
import androidx.lifecycle.ViewModelProviders
import com.danthecodinggui.framework.R
import com.danthecodinggui.framework.di.injectables.DaggerAppCompatActivity
import com.danthecodinggui.framework.di.view_model.AppViewModelFactory
import javax.inject.Inject

class ExampleActivity : DaggerAppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: AppViewModelFactory
    private lateinit var viewModel: ExampleViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        viewComponent.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(ExampleViewModel::class.java)
    }
}
